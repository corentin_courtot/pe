# Contexte

Ce repository contient le travail réalisé par Baptiste Lefevre et Corentin Courtot au cours de notre dernière année d'étude à l'école des mines de Saint-Etienne dans le cadre de la réalisation d'un projet de fin d'étude intitulé "Etude de la consommation d'une IA". L'objectif de ce projet était de mesurer la consommation énergétique d'une carte électronique de type STM32 lors de l'utilisation de plusieurs modèles de réseaux de neurones différents, de les comparer entre eux et de déterminer quelle fonction ou couche d'un réseau impacte le plus sa consommation. Nous voulions également étudier dans quelle mesure cette consommation pourrait être réduite à l'aide d'optimisations logicielles (quantization, pruning,...). Ce projet a été proposé par Mr Baptiste N'Guyen pour l'aider dans la réalisation de sa thèse sur des objets autonomes en énergie embarquant des réseaux de neurones.

Dans ce Readme, nous allons présenter le contenu du repository, expliquer comment répliquer nos manipulations pour mesurer le courant consommé par une carte lors de l'inférence d'un modèle, et exposer les résultats de nos tests.

#### Note :

L'ensemble des réseaux de neurones testés ont été conçus pour la base de donnée German Traffic Sign Recognition Benchmark, composé d'image RGB de 32\*32 (inputs_shape=[32, 32, 3]), et de 43 classes.

# Présentation des fichiers

Ce repository contient 2 dossiers :

-   Le dossier _STM32CubeProjects_ contenant le projet STM32Cube utilisé pour upload un modèle sur la carte STM32 testée.
-   Le dossier _ScriptsPython_ contenant les données acquises et les scripts utilisés pour les exploiter.

## Le dossier ScriptsPython

Ce dossier regroupe des fichiers python, et des sous-dossiers. Les sous-dossiers contiennent les informations suivantes :

-   _AnalyseData_ : données et graphiques de chaque modèle testé.
-   _Modules_ : petits scripts python définissant des fonctions ou des classes utilisés dans les autres scripts.
-   _OutputData_ : Graphiques finaux après traitement des données.
-   _TensorFlowModel_ : fichiers repésentant les réseaux de neurones pour le software pack STM32CubeAI, les données d'entraînement et de test du réseau.

### Fonctions réalisées par les scripts

-   InstallPackages.py : installe tous les packages python utilisés par nos scripts
-   STM32model.py : génération d'un model tensorflow représentant le réseau de neurones à tester. Ce modèle est stocké au format .h5.
-   Communication.py : gère la communication entre la carte STM32 et le PC lors de la phase de test. Le PC envoie les images à la carte, la carte réalise l'inférence avec le modèle chargé, et renvoie sa prédiction. Au cours de nos tests, nous transmettions 50 images différentes prises au hasard, mais à chaque fois les mêmes. C'est pendant cette phase qu'on mesure le courant consommé par la carte grâce à la plateforme.
-   HandleData.py : A l'issue de la phase de test, on récupère un fichier au format .stpm avec le logiciel utilisé pour les mesures (STM32CuboMonitorPower). Ce script traite ce fichier pour générer des courbes avec matplotlib. La courbe la plus importante et contenant le plus d'informations est la courbe intitulée "Courbe typique.png". Cette courbe est générée en prenant la moyenne des tests de consommation avec les 50 images différentes.
-   VisualizeData.py : Récupère toutes les données intéressantes de chacun des modèles et génère de nouveaux graphes afin de pouvoir comparer les modèles entre eux.

Pour assurer le bon fonctionnement de chacun des scripts, les exécuter depuis le dossier _SciptsPython_.

### Nomenclature utilisée

Nous avons utilisé une nomenclature particulière pour distinguer les différents modèles testés. Cette nomenclature est importante à comprendre car les fichiers de données (dans _AnalyseData/Data_) et les graphiques générés par nos scripts (dans _AnalyseData/Graphs_) sont dans des sous-dossiers utilisant cette nomenclature, et elle est aussi utilisée pour distinguer les modèles dans la partie résultats. Elle fonctionne comme suit :

-   indiquer chaque couche du modèle (dans l'ordre input -> output) en écrivant sa première lettre (couche Dense -> 'd', couche Convolutionnelle -> 'c'). Une couche d'activation de type 'ReLu' entre 2 couches denses peut être omise car considérée comme par défaut, tout comme un flatten entre une couche convolutionnelle et une couche dense.
-   inscrire chaque paramètre susceptible de varier à la suite sous forme de chiffre (pool_size, quantité de neurones dans une couche dense, ...).

Par exemple, un modèle composé des couches ci-dessous serait nommé _c6p22c16p22d120d43softmax_

```
x = inputs(inputs_shape=[32, 32, 3])
x = Conv2D(filters=6, kernel_size=5, activation='linear', use_bias=True)(x)
x = MaxPooling2D(pool_size=(2, 2))(x)
x = Conv2D(filters=16, kernel_size=5, activation='linear', use_bias=True)(x)
x = MaxPooling2D(pool_size=(2, 2))(x)
x = Flatten()(x)
x = Dense(units=120, activation='linear', use_bias=True)(x)
x = Activation('relu')(x)
x = Dense(units=43, activation='linear', use_bias=True)(x)
x = Activation('softmax')(x)
```

# Tutoriel d'utilisation

Cette partie décrit la procédure que nous avons suivie afin d'obtenir des données de consommation de courant d'une carte STM32L401re lors de l'inférence pour plusieurs modèles de réseaux de neurones.

Avant de commencer, si c'est la première ouverture de ces scripts, exécuter le script InstallPackages.py, qui devrait installer plusieurs modules python indispensables pour la suite.

## Générer et upload un modèle de réseau de neurone sur la carte

-   **Modifier le fichier STM32model.py**, et notamment **la fonction Lenet5(classe, inputs_shape)** pour qu'elle décrive le modèle testé. **Entrer le nom du modèle dans la variable 'structure_model'**.
-   **Executer le script STM32model.py**. Ce script devrait générer un fichier _lenet5\_'structure_model'.h5_.
-   **Ouvrir le projet STM32Cube** (voir dossier _STM32CubeProjects/STM32AI_). Grâce au software pack _STM.X-CUBE-AI.5.2.0_, ajouter le fichier _lenet5\_'structure_model'.h5_ à _Model_.
-   **Brancher une carte STM32** à votre PC, et **run** le projet STM32Cube. Le logiciel devrait regénérer le code pour le nouveau modèle et l'upload sur la carte STM32.

## Prendre des mesures de consommation

Vous avez chargé un modèle sur la carte stm testée. Vous souhaitez maintenant acquérir des données de consommation de courant au cours de l'inférence.

-   **brancher la carte testée à la plateforme de mesure** et **configurer le logiciel de mesure** pour aquérir des données (voir tutoriel d'utilisation de STM32CubePowerMonitor : https://docs.google.com/document/d/1QAFzkB69f2fFzQgcqn6YW7L7r4FzrJsOSsJqOU3MjXw/edit?usp=sharing).
-   **brancher la carte testée au PC** et s'assurer de la **position du jumper** de la carte pour que l'alimentation de celle-ci se fasse bien via la plateforme de mesure (pour stm32f4xx : jumper en position E5V). **Modifier la variable 'port'** dans le script Communication.py pour qu'elle corresponde au port auquel la carte est branchée.
-   **Lancer l'acquisition** des données avec la plateforme de mesure, puis **exécuter le script** Communication.py.
-   **Attendre** que la carte testée réalise l'inférence avec le modèle uploadé avec les 50 images de test.
-   Arrêter l'acquisition des données, et sauvegarder les données acquises via STM32CubePowerMonitor dans le dossier _ScriptsPython/AnalyseData_.

Vos mesures ont été sauvegardées. Vous pouvez maintenant passer à l'étape 'Visualiser les données'.

## Visualiser les données

Vous avez terminé l'acquisition des données, et vous avez un fichier au format .stpm contenant les mesures. Vous souhaitez maintenant visualiser les données.

-   **Ouvrir** le script HandleData.py. Entrer dans la variable 'modelName' le nom du modèle testé, puis **exécuter ce script**.
-   Après avoir créé les dossier qui vont contenir les fichiers de données, le script demande de déplacer le fichier \*.pckl qui doit être dans _ScriptsPython/Modules_ vers le dossier _ScriptsPython/AnalyseData/Data/"modelName"_, puis de mettre le fichier au format .stpm généré via stm32CubePowerMonitor dans _ScriptsPython/AnalyseData_, ce qui devrait être déjà fait.
-   **Appuyer sur Entrée** après avoir fait ce que demande le script. Il reprend son exécution, et le dossier _AnalyseData/Data/"modelName"_ devrait être rempli avec les données brutes dézippées (exécution du script AnalyseData/UnzipStpm.py), et _AnalyseData/Graphs/"modelName"_ devrait contenir des graphs permettant d'avoir un apercu des données mesurées, ainsi que des objets npy utilisés lors de la comparaison de modèles entre eux (exécution du script Modules/Analyse.py).

Vous pouvez maintenant accéder au dossier _AnalyseData/Graphs/"modelName"_, qui contient des images des graphiques intéressants. Pour avoir les graphes directement, vous pouvez éditer la fonction analyse() dans Modules/Analyse.py. Cela ne nous intéressait pas car nous avons comparé les données de plusieurs modèles entre eux, ce qui est expliqué dans la partie suivante.

## Comparer les données de plusieurs modèles entre elles

Vous disposez des données traitées de plusieurs modèles, et vous souhaitez maintenant les comparer.

C'est le script VisualiseData.py qui vous permettra de comparer les données entre elles, mais il va falloir **l'éditer en fonction de ce que vous voulez visualiser**.

Visualiser des données est simplifié grâce à la fonction **createPlot()**, prenant en argument un titre de graphique et la liste des modèles à plot. Cette fonction se chargera aussi d'enregistrer le graph généré dans le dossier _ScriptsPython/OutputData_. Il y a plusieurs exemples d'utilisations de cette fonction dans le script VisualizeData.py.

# Nos Résultats

Dans cette partie, nous allons vous présenter les résultats de notre travail ainsi que notre démarche. Toutes les images présentées dans cette partie sont disponibles dans le dossier _ScriptsPython/OutputData_.

Nous avons travaillé à partir d'un modèle ayant des performances correctes sur notre base de donnée : c6p22c16p22d120d84d43softmax. Dans la suite, ce modèle sera désigné comme "modèle de base", puisque nous avons testé plusieurs cas de figure en prenant celui-ci comme référence.

Dans un premier temps, afin de valider la pertinence de nos mesures, nous avons essayé de **mesurer à 3 reprises la consommation du modèle de base**.

![Screenshot](ScriptsPython/OutputData/Comparing3differentsmeasurementsofthesamemodel.png)

Ces mesures nous indiquent que :

-   Un léger offset en courant n'est pas significatif et est probablement lié à la configuration de la plateforme.
-   Le pic de courant en fin d'inférence (ici à environ 105 ms) n'est pas significatif non plus, et doit plutôt être lié à la transmission du résultat de l'inférence au PC.

Nous avons ensuite voulu vérifier que nous obtenions bien des **mesures très distinctes à partir de modèles différents**. Le graphique suivant compare donc le modèle de base avec un modèle extrêmement simple : un GlobalMaxPooling, suivi d'une couche dense de 43 neurones, et d'une couche d'activation softmax pour obtenir la sortie.

![Screenshot](ScriptsPython/OutputData/Basemodelvssmallestpossiblemodel.png)

Cet essai a fourni le résultat attendu : l'inférence est beaucoup plus rapide et moins consommatrice pour le modèle simple.

Puis nous avons essayé de connaître l'influence des **couches denses**. Nous avons donc conduits 3 tests :

-   Ajouter des couches denses une par une à un modèle très simple.
    ![Screenshot](ScriptsPython/OutputData/Incrementaladditionoffullyconnectedlayersonsimplestmodel.png)

-   Modifier le nombre de neurones au sein d'une couche dense.
    ![Screenshot](ScriptsPython/OutputData/120vs1000neuronsinfullyconnectedlayer.png)

-   Réduire progressivement le nombre de couches denses du modèle de base.
    ![Screenshot](ScriptsPython/OutputData/Effectoftheremovaloffullyconnectedlayers.png)

Nous pouvons voir assez clairement les changements de consommation, et ceux-ci semblent assez cohérents : plus les couches denses comportent de paramètres, plus elles coûtent en énergie et en temps.

Puis nous avons essayé de mettre en évidence l'impact sur la consommation des **couches convolutionnelles**. Pour ce faire, nous avons réalisé les tests suivants :

-   Comparer une convolution la plus simple possible (filter=1, kernel_size=5) à un GlobalMaxPooling.
    ![Screenshot](<ScriptsPython/OutputData/EffectsofGlobalMaxPoolingvsConvolution(filter=1,kernel_size=5).png>)

-   Modifier les couches convolutionnelles du modèle de base.
    ![Screenshot](ScriptsPython/OutputData/Modifyingconvlayersonbasemodel.png)

Ces tests ont été assez peu concluants, nous avons pu émettre quelques hypothèses sans aboutir à une analyse précise.

Nous avons enfin voulu déterminer si la **fonction d'activation** 'softmax' avait un coût énergétique élevé. Les tests suivants nous ont permis de tester ce point :

-   Remplacer le softmax du modèle de base par une activation linéaire, supposée d'une complexité à calculer largement inférieure.
    ![Screenshot](ScriptsPython/OutputData/Linearvssoftmax.png)

-   Appliquer un softmax aux données de sortie 2 fois.
    ![Screenshot](ScriptsPython/OutputData/Basemodelvsbasemodelwithsoftmaxappliedtwice.png)

Les différences étaient ici très faibles, ce qui n'a pas permis de mettre en évidence une consommation supérieure pour la fonction softmax, et indique même qu'elle n'a qu'un très petit impact sur la consommation.

Enfin, nous avons testé une des nombreuses technique d'optimisation : la **quantization**. Pour évaluer son impact, nous avons comparé le modèle de base avec et sans quantization.
![Screenshot](ScriptsPython/OutputData/Effectsof8bitquantizationonbasemodel.png)

Ce test semble indiquer que la quantization a un impact non négligeable sur les couches denses, bien que d'autres tests soient requis pour tirer des conclusions plus précises.

# Conclusion

Les tests réalisés nous ont permis de mettre à l'épreuve certaines de nos hypothèses et de récolter des données intéressantes, sans pour autant nous permettre de conclure précisément. D'autres tests, peut-être avec des bases de données plus simples, avec des données d'entrée plus petites, seraient pertinents. Néanmoins, notre présence à l'école ayant été limitée à deux jours par semaine au cours de ce projet, nous n'avons pas pu tous les conduire.

Nous n'avons malheureusement pu que très peu aborder les techniques d'optimisation de réseaux de neurones, ce qui était initialement l'objectif principal du projet.

Malgré ces limitations, nous estimons avoir fait au mieux au vu des conditions de travail. De plus, nous pensons que notre code pourra être relativement facilement réutilisé pour de futures mesures avec des modèles différents et devrait être utile aux travaux de Mr Baptiste N'Guyen.

Enfin, nous transmettons quelques **questions ouvertes** auxquelles nous n'avons pas pu apporter de réponse à l'issue de notre projet, adressées aux personnes souhaitant poursuivre notre travail.

-   Causes de l'offset de la mesure de courant?
-   Lors des mesures de courant, débrancher le cable reliant la board et le PC pour la communication cause une grosse différence de mesure de courant, supposant que le cable servant à la communication transmet de l'énergie à la carte alors que l'alimentation devrait intégralement provenir de la plateforme de mesure.

# Remerciements

Merci à **Baptiste N'Guyen** et **Pierre-Alain Moellic** pour avoir suivi notre projet.

Merci à l'**ID-Fab** des Mines de Saint-Etienne à Gardanne pour nous avoir fourni le matériel nécessaire, ainsi qu'un bon environnement de travail.
