def unzipData(modelName):
    from pyunpack import Archive
    import os

    for file in os.listdir('./AnalyseData'):
        if file.endswith(".stpm"):
            Archive('./AnalyseData/' + file).extractall(
                'AnalyseData/Data/' + modelName + '/unzippedFiles')
