#!/usr/bin/env python

""" 
Edited by Baptiste Lefevre and Corentin Courtot

This script handles communication between device under test and PC while testing.
It will send images one by one from the PC to the device, wait for the device to complete inference, compare the device output with expected result and start over.
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 13:54:45 2020

@author: BN262210
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 10:52:02 2020

@author: Baptiste
"""


import pickle
import serial
import re
import numpy as np
import struct
import time as t
from Modules.customStates import *
port = "COM23"
baud = 115200


try:
    x_train = np.load("TensorFlowModel/x_train_stm32test.npy")
    y_train = np.load("TensorFlowModel/y_train_stm32test.npy")
    x_test = np.load("TensorFlowModel/x_test_stm32test.npy")
    y_test = np.load("TensorFlowModel/y_test_stm32test.npy")
except:
    import os
    import sys
    from pathlib import Path
    from Modules.loadDataset import *
    parentpath = str(Path(os.getcwd()).parent)
    sys.path.append(parentpath)
    load_dataset_path = os.getcwd()+os.sep
    x_train, y_train, x_test, y_test = load_gtsrb(load_dataset_path, True)
    np.save("TensorFlowModel/x_train_stm32test.npy", x_train)
    np.save("TensorFlowModel/y_train_stm32test.npy", y_train)
    np.save("TensorFlowModel/x_test_stm32test.npy", x_test)
    np.save("TensorFlowModel/y_test_stm32test.npy", y_test)


def log(event):
    print(t.time(), event)


def synchronisation_with_target(debug=False):
    sync = False
    ret = None

    while (sync == False):
        ser.write(b"sync")
        ret = ser.read(3)
        if (ret == b"101"):  # "101" has been chosen arbitrarily
            sync = True
            if (debug):
                print("Synchronised")
        else:
            if (debug):
                print("Wrong ack reponse")


graph = CustomStates()
graph.currentState = graph.states["sync"]
with serial.Serial(port, baud, timeout=1) as ser:
    if ser.isOpen():
        print(ser.name + ' is open...')

    nb_echant = 50
    sucess = 0
    for l in list(range(0, nb_echant)):
        print("\nEchantillon ", l, "/", nb_echant, sep='')
        log("start sync")
        graph.change("sync")
        synchronisation_with_target(False)
        log("end   sync")

        panneau = x_test[l]
        ser.flush()
        log("start sending")
        graph.change("send")
        for i in list(range(0, 32)):
            for j in list(range(0, 32)):
                for k in list(range(0, 3)):
                    ser.write(panneau[i, j, k])
        log("end   sending")

        log("wait  post-proc signal")
        graph.change("wait")
        out_ack = b"000"
        while(out_ack != b"010"):  # "010" has been chosen arbitrarily
            out_ack = ser.read(3)
        log("got   post-proc signal")

        received_output = np.zeros((1, 43))
        log("start receiving")
        graph.change("receive")
        for i in range(43):
            received_output[0][i] = struct.unpack('f', ser.read(4))[0]
        log("end  receiving")

        prediction = np.argmax(received_output, axis=None, out=None)
        realite = np.argmax(y_test[l], axis=None, out=None)

        if prediction == realite:
            sucess += 1
            print("ok d'où", int(sucess/(l+1)*100), "%")
        else:
            print(realite, "vu comme", prediction,
                  "d'où", int(sucess/(l+1)*100), "%")
graph.change("sync")

print(sucess/nb_echant*100, "% sur les", nb_echant, "essais")
#model = tf.keras.models.load_model("lenet5.h5")
# print(model.predict([panneau]))


f = open('Modules/store_python_states.pckl', 'wb')
pickle.dump(graph, f)
f.close()
