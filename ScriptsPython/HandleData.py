#!/usr/bin/env python

""" 
@author: Baptiste Lefevre and Corentin Courtot

This script is supposed to be called just after recording data with STM32CubeMonitorPower.
It creates folder for the model under test, unzips raw data and calls 'analyseData' in 'Modules/Analyse.py'. 
analyseData will process raw data and generate relevant graphs. See Analyse.py for more info.
Just replace the variable 'modelName' with the name of the model.
"""


import os
import AnalyseData.UnzipStpm
import Modules.Analyse

# Set modelName variable to the name of the model under test
modelName = 'c6p22c16p22d43softmax'
modelName = 'c6p22c16p22d120d43softmax'
modelName = 'c6p22c16p22d120d84d43softmax'
modelName = 'c6p22c16p22c32d120d84d43softmax'

modelName = 'GMPd43softmax'
modelName = 'c6p22c16p22d120d84d43linear'
modelName = 'c32p22c16p22d120d84d43softmax'

modelName = 'c6p22c16p22d120d84d43softmax2'
modelName = 'GMPd120d43softmax'
modelName = 'GMPd120d120d120d43softmax'
modelName = 'GMPd1000d43softmax'
modelName = 'quantized8_c6p22d120d120d43softmax'
modelName = 'c1p22d120d43softmax'

modelName = 'quantized8_c6p22c16p22d120d84d43softmax'

modelName = 'quantized8_GMPd120d120d120d43softmax'

cpt = 0
# creating model directories
try:
    os.mkdir('AnalyseData/Data/' + modelName)
except FileExistsError:
    print("Directories for ", modelName,  " already exist")
    cpt += 1

try:
    os.mkdir('AnalyseData/Data/' + modelName + '/unzippedFiles')
except FileExistsError:
    print("Directories for ", modelName,  " already exist")
    cpt += 1

try:
    os.mkdir('AnalyseData/Graphs/' + modelName)
except FileExistsError:
    print("Directories for ", modelName,  " already exist")
    cpt += 1

if cpt < 3:  # if the model tested does not yet have all corresponding folders, hence it has been tested for the first time
    xxx = input(
        "Déplacer le fichier *.pckl qui doit etre dans 'Modules' vers le dossier 'AnalyseData/Data/" + modelName + "'")
    xxx = input(
        "Mettre le fichier *.stpm généré via stm32CubePowerMonitor dans 'AnalyseData'")

    # unzip *.stpm file in 'AnalyseData/Data/'modelName'/unzippedFiles'
    AnalyseData.UnzipStpm.unzipData(modelName)

# run script that generate relevant data from raw data stored in unzippedFiles. See Module/Analyse.py
Modules.Analyse.analyse(modelName)
