#!/usr/bin/env python

""" 
@author: Baptiste Lefevre and Corentin Courtot

Installs all python packages needed for this project
"""


import subprocess
import sys


def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])


install('numpy')
install('tensorflow')
install('serial')
install('pyserial')
install('matplotlib')
install('pyunpack')
