#!/usr/bin/env python

""" 
@author: Baptiste Lefevre and Corentin Courtot

defines analyse() function, which is called in HandleData.py script.

"""


from Modules.customStates import *
import numpy as np
import pickle
import glob
import matplotlib.pylab as plt


def analyse(modelName):
    """
    Takes raw data of inference measurements in *.csv files and turns it into usable and easier to visualize data.
    Generates a few graphs in the process.
    The main goal of this function is to generate an average consumption profile of an inference over the 50 images.
    To do so, it will detect the date at which each 50 inferences begin, and average the consumption of the 50 inferences during the 500 following ms.
    That average is then stored in 'AnalyseData/Graphs/'+modelName+'/T_typic' and 'AnalyseData/Graphs/'+modelName+'/I_typic'.
    That average will be used in other files used to compare results between every model consumptions.
    All further plots are only used to check if measurements went according to expected.
    """

    f = open('AnalyseData/Data/' + modelName +
             '/store_python_states.pckl', 'rb')
    print(f)
    graph = pickle.load(f)
    f.close()

    # loading in T (for Time, in ms) and I (for Intensity, in uA) the raw data measured unzipped
    T = []
    I = []
    cpt = 1
    found = True
    while found:
        found = False
        for file in glob.glob('AnalyseData/Data/'+modelName+'/unzippedFiles/*'):
            # print("search",str(cpt),"in",file.split('.')[0].split('_')[-1])
            if 'rawfile' in file and str(cpt) == file.split('.')[0].split('_')[-1]:
                found = True
                cpt += 1
                f = open(file, encoding='utf-8')
                for line in f:
                    T.append(float(line.split(';')[0]))
                    I.append(float(line.split(';')[1]))

    # convert T and I into np arrays
    T = np.array(T)
    I = np.array(I)

    # displays raw data in a plot
    title = "Consumption profile"
    plt.figure(title)
    plt.title(title)
    plt.xlabel('Time (ms)')
    plt.ylabel('Consumption (uA)')
    plt.plot(T, I)
    plt.show(block=False)
    plt.savefig('AnalyseData/Graphs/'+modelName+'/'+title+'.png')

    # detection of beginning of inference
    dates_trigger = []
    id_trigger = []
    cpt = 0
    for i in range(1, len(I)):
        if I[i] - I[i-1] > 1400:
            if len(dates_trigger) == 0 or T[i] - dates_trigger[-1] > 500:
                dates_trigger.append(T[i-10])
                id_trigger.append(i-10)

    dates_trigger = dates_trigger[1:-2]
    id_trigger = id_trigger[1:-2]

    # pts : nb of points considered after trigger. freq = 10 000Hz, hence pts=5000 => 500ms.
    pts = 5000
    nb_courbes = len(dates_trigger)

    I_typic = np.zeros(pts)
    T_typic = []

    # displaying relevant data
    title = "Events"
    plt.figure(title)
    plt.title(title)
    plt.xlabel('Time (ms)')
    plt.ylabel('Consumption (uA)')
    for id_t in id_trigger:
        T_evt = [T[i]-T[id_t] for i in range(id_t, id_t+pts)]
        I_evt = [I[i] for i in range(id_t, id_t+pts)]
        I_typic += np.array(I_evt) / nb_courbes
        T_typic = T_evt
        plt.plot(T_evt, I_evt)
    plt.show(block=False)
    plt.savefig('AnalyseData/Graphs/'+modelName+'/'+title+'.png')

    title = "Average inference consumption"
    plt.figure(title)
    plt.title(title)
    plt.xlabel('Time (ms)')
    plt.ylabel('Consumption (uA)')
    plt.plot(T_typic, I_typic)
    plt.show(block=False)
    plt.savefig('AnalyseData/Graphs/'+modelName+'/'+title+'.png')

    np.save('AnalyseData/Graphs/'+modelName+'/T_typic', T_typic)
    np.save('AnalyseData/Graphs/'+modelName+'/I_typic', I_typic)

    title = "Consumption profile with trigger points"
    plt.figure(title)
    plt.title(title)
    plt.xlabel('Time (ms)')
    plt.ylabel('Consumption (uA)')
    plt.plot(T, I)
    for id_t in id_trigger:
        plt.scatter(T[id_t], I[id_t])
    plt.show(block=False)
    plt.savefig('AnalyseData/Graphs/'+modelName+'/'+title+'.png')

    np.save('AnalyseData/Graphs/'+modelName+'/T_complet', T)
    np.save('AnalyseData/Graphs/'+modelName+'/I_complet', I)

    # Etats
    graph.plot()

    # Align Python and Conso
    first_wait = 0
    while graph.pointsY[first_wait] != 2:  # detection of 1st wait state in graph
        first_wait += 1
    date_first_wait = graph.pointsX[first_wait]

    title = "Align PythonCommunication and Consomption"
    plt.figure(title)
    plt.title(title)
    plt.xlabel('Time (ms)')
    plt.ylabel('Consumption (uA)')
    plt.plot((np.array(graph.pointsX)-date_first_wait)*1000 +
             dates_trigger[0], (np.array(graph.pointsY)*2000+20000))
    plt.plot(T, I)
    plt.show(block=False)
    plt.savefig('AnalyseData/Graphs/'+modelName+'/'+title+'.png')
