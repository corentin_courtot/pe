#!/usr/bin/env python

"""
Created 2021/03/10

@author: Baptiste Lefevre

"""

import time as t
import matplotlib.pylab as plt
import numpy as np


class CustomStates:
    """
    Class recording the state of the Communication.py script while communicating with board.
    Used in order to make sure we were effectively recording inference.
    While the board is performing inference, communication state is 'wait'.
    """

    def __init__(self):
        self.currentState = None
        self.states = {'sync': 0, 'send': 1, 'wait': 2, 'receive': 3}
        self.pointsX = []
        self.pointsY = []

    def change(self, newState):
        date = t.time()
        self.pointsX.append(date)
        self.pointsY.append(self.currentState)
        self.currentState = self.states[newState]
        self.pointsX.append(date)
        self.pointsY.append(self.currentState)

    def plot(self):
        y_axis = ['Sync', 'Send', 'Wait', 'Receive']

        plt.figure("Etats de Python")

        #plt.yticks(self.pointsY, y_axis)
        plt.plot(self.pointsX, self.pointsY)

        locs, labels = plt.yticks()
        #yticks(np.arange(0, 3, step=1))
        plt.yticks(np.arange(4), ('Sync', 'Send', 'Wait', 'Receive'))

        plt.show()
