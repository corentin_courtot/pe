#!/usr/bin/env python

"""
Created 2021/03/10

@author: Baptiste Lefevre

Generates a Tensorflow model with basic defaults features

"""

import time as t
import numpy as np
from Modules.loadDataset import *
from tensorflow.keras.layers import *
from tensorflow.keras import datasets, layers, models
import tensorflow.keras as keras
import tensorflow as tf
import os


class TfR_model:
    """
    Class used to quicky generate model from name.

    Use case :

    structure_model = "c1p22d120d43softmax"
    etfr_model = etfr.TfR_model(structure_model)
    model = etfr_model.compile()

    After this code, 'model' is the model associated to the name 'c1p22d120d43softmax'
    """

    def __init__(self, input, debug=False):
        self.input = input
        self.debug = debug
        self.layers = []
        self.human_layers = []

        self.gen_layers()
        self.complete_layers()

    def gen_layers(self):

        s = self.input
        input_shape = [32, 32, 3]

        lay = tf.keras.Input(shape=input_shape)
        self.add_layer(lay, 'input')

        over = False
        on_layer = False
        params = []
        mult_params = True
        curr_layer = None
        i = 0
        while not over:
            if len(s)-i >= len("softmax") and s[i:i+len("softmax")] == "softmax":
                if self.debug:
                    print("OK LAYER", params)
                if curr_layer == 'conv':
                    self.add_layer(tf.keras.layers.Conv2D(filters=params[0],
                                                          kernel_size=5, activation='linear', use_bias=True), 'Conv2D')
                elif curr_layer == 'pooling':
                    self.add_layer(tf.keras.layers.MaxPooling2D(
                        pool_size=(params[0], params[1])), 'MaxPooling2D')
                elif curr_layer == 'dense':
                    self.add_layer(tf.keras.layers.Dense(
                        units=params[0], activation='linear', use_bias=True), 'Dense')
                # print("Nan")
                on_layer = False
                mult_params = True
                params = []
                #lay = tf.keras.layers.Activation('softmax')
                self.add_layer(tf.keras.layers.Activation(
                    'softmax'), 'softmax')
                i += len("softmax")
                if self.debug:
                    print("softmax")
                if self.debug:
                    print("OK LAYER")
            if len(s)-i >= len("flatten") and s[i:i+len("flatten")] == "flatten":
                if self.debug:
                    print("OK LAYER", params)
                if curr_layer == 'conv':
                    self.add_layer(tf.keras.layers.Conv2D(filters=params[0],
                                                          kernel_size=5, activation='linear', use_bias=True), 'Conv2D')
                elif curr_layer == 'pooling':
                    self.add_layer(tf.keras.layers.MaxPooling2D(
                        pool_size=(params[0], params[1])), 'MaxPooling2D')
                elif curr_layer == 'dense':
                    self.add_layer(tf.keras.layers.Dense(
                        units=params[0], activation='linear', use_bias=True), 'Dense')
                # print("Nan")
                on_layer = False
                mult_params = True
                params = []
                #lay = tf.keras.layers.Activation('softmax')
                self.add_layer(tf.keras.layers.Flatten(), 'flatten')
                i += len("flatten")
                if self.debug:
                    print("flatten")
                if self.debug:
                    print("OK LAYER")

            elif s[i] in ["c", "p", "d"] and not on_layer:
                on_layer = True
                if s[i] == "c":
                    if self.debug:
                        print("convolution")
                    curr_layer = 'conv'
                elif s[i] == "p":
                    if self.debug:
                        print("pooling")
                    curr_layer = 'pooling'
                    mult_params = False
                elif s[i] == "d":
                    if self.debug:
                        print("dense")
                    curr_layer = 'dense'
                else:
                    if self.debug:
                        print("oups layer ?")
                i += 1
            elif on_layer:
                try:
                    v = int(s[i])
                    # print("number",s[i])
                    if mult_params:
                        if len(params) > 0:
                            params[0] = params[0]*10 + v
                        else:
                            params.append(v)
                    else:
                        params.append(v)
                    i += 1
                    # print(params)
                except:
                    if self.debug:
                        print("OK LAYER", params)
                    if curr_layer == 'conv':
                        self.add_layer(tf.keras.layers.Conv2D(filters=params[0],
                                                              kernel_size=5, activation='linear', use_bias=True), 'Conv2D')
                    elif curr_layer == 'pooling':
                        self.add_layer(tf.keras.layers.MaxPooling2D(
                            pool_size=(params[0], params[1])), 'MaxPooling2D')
                    elif curr_layer == 'dense':
                        self.add_layer(tf.keras.layers.Dense(
                            units=params[0], activation='linear', use_bias=True), 'Dense')
                    # print("Nan")
                    on_layer = False
                    mult_params = True
                    params = []
            if i >= len(s):
                over = True

    def add_layer(self, layer, human_layer):
        self.layers.append(layer)
        self.human_layers.append(human_layer)

    def complete_layers(self):
        i = 0
        while i < len(self.layers)-1:
            if self.human_layers[i] == 'Conv2D' and self.human_layers[i+1] == 'MaxPooling2D':
                # add acti relu
                self.layers.insert(i+1, tf.keras.layers.Activation('relu'))
                self.human_layers.insert(i+1, 'relu')
            elif self.human_layers[i] == 'Dense' and self.human_layers[i+1] == 'Dense':
                # add acti relu
                self.layers.insert(i+1, tf.keras.layers.Activation('relu'))
                self.human_layers.insert(i+1, 'relu')
            elif self.human_layers[i] == 'MaxPooling2D' and self.human_layers[i+1] == 'Dense':
                # add flatten
                self.layers.insert(i+1, tf.keras.layers.Flatten())
                self.human_layers.insert(i+1, 'flatten')
            i += 1

    def print_layers(self):
        for lay, h_lay in zip(self.layers, self.human_layers):
            print(h_lay, lay)

    def compile(self):
        x = self.layers[0]
        for i in range(1, len(self.layers)):
            if self.debug:
                print()
            if self.debug:
                print(self.layers[i])
            if self.debug:
                print(self.layers[i-1])
            x = self.layers[i](x)
        return tf.keras.Model(inputs=self.layers[0], outputs=x)
