# Python Scripts

Ce dossier regroupe les fichiers python utilisés pour réaliser les mesures de consommation de la carte ST lors de l'inférence d'un réseau de neurone.

L'ensemble des réseaux de neurones testés ont été concus pour la base de donnée **_TO COMPLETE_**, composé d'image RGB de 32\*32 (inputs_shape=[32, 32, 3]), et de 43 classes.

#### Note importante :

Nous avons utilisé une nomenclature particulière au cours de ce projet pour distinguer les différents modèles testés. Cette nomenclature est importante à comprendre car les fichiers de données et les graphiques générés par nos scripts sont dans des dossier utilisant cette nomenclature, et elle est aussi utilisée pour distinguer les modèles dans la partie résultats. Elle fonctionne comme suit :

-   indiquer chaque couche du modèle (dans l'ordre input -> output) en écrivant sa première lettre (couche Dense -> 'd', couche Convolutionnelle -> 'c'). Une couche d'activation de type 'ReLu' entre 2 couches denses peut être omise car considérée comme par défault, tout comme un flatten entre une couche convolutionnelle et une couche dense.
-   inscrire chaque paramètre susceptible de varier à la suite sous forme de chiffre (pool_size, quantite de neurones dans une couche dense, ...).

Par exemple, un modèle composé des couches ci-dessous serait nommé _c6p22c16p22d120d43softmax_

```
x = inputs(inputs_shape=[32, 32, 3])
x = Conv2D(filters=6, kernel_size=5, activation='linear', use_bias=True)(x)
x = MaxPooling2D(pool_size=(2, 2))(x)
x = Conv2D(filters=16, kernel_size=5, activation='linear', use_bias=True)(x)
x = MaxPooling2D(pool_size=(2, 2))(x)
x = Flatten()(x)
x = Dense(units=120, activation='linear', use_bias=True)(x)
x = Activation('relu')(x)
x = Dense(units=43, activation='linear', use_bias=True)(x)
x = Activation('softmax')(x)
```

# Fonctions réalisées

-   installPackages : installe tous les packages python utilisés par nos scripts
-   STM32model.py : generation d'un model tensorflow représentant le réseau de neurone à tester. Ce modèle est stocké au format .h5. Nous avons utilisé une nomenclature particulière pour se rappeler quel fichier corespond à quel modèle de réseau de neurone (voir Modules/easyTfRedactor.py).
-   Communication.py : gère la communication entre la carte STM32 et le PC lors de la phase de test. Le PC envoie les images à la carte, la carte réalise l'inférence avec le modèle chargé, et renvoie sa prédiction. Nous testions avec 50 images différentes prises au hasard, mais à chaque fois les mêmes. C'est pendant cette phase qu'on mesure le courant consommé par la carte grâce à la plateforme.
-   HandleData.py : A l'issue de la phase de test, on récupère un fichier au format .stpm avec le logiciel utilisé pour les mesures (STM32CuboMonitorPower). Ce script traite ce fichier pour générer des courbes avec matplotlib. La courbe la plus importante et contenant le plus d'informations est la courbe intitulée "Courbe typique.png". Cette courbe est générée en prenant la moyenne des tests de consommation avec les 50 images différentes.
-   VisualizeData.py : Récupère toutes les données intéressantes de chacuns des modèles et génère de nouveaux graphes afin de pouvoir comparer les modèles entre eux.

# Comment utiliser ces scripts?

Cette partie décrit la procédure que nous avons suivie afin d'obtenir des données de consommation de courant d'une carte STM32L401re lors de l'inférence pour plusieurs modèles de réseaux de neurones.

Avant de commencer, si c'est la première ouverture de ces scripts, exécuter le script InstallPackages.py, qui devrait installer plusieurs modules python indispensable pour la suite.

## Pour générer et upload un modèle de réseau de neurone sur la carte

-   **Modifier le fichier STM32model.py**, et notamment **la fonction Lenet5(classe, inputs_shape)** pour qu'elle décrive le modèle devant être testé. **Entrer le nom du modèle dans la variable structure_model** pour mieux vous y retrouver.
-   **Executer le script STM32model.py**. Ce script devrait générer un fichier _lenet5\_'structure_model'.h5_.
-   **Ouvrir le projet STM32Cube** (voir dossier _STM32CubeProjects/STM32AI_). Grâce au software pack _STM.X-CUBE-AI.5.2.0_, ajouter le fichier _lenet5\_'structure_model'.h5_ à _Model_.
-   **Brancher une carte STM32** à votre PC, et **run** le projet STM32Cube. Le logiciel devrait regénérer le code pour le nouveau modèle et l'upload sur la carte STM32.

## Pour prendre des mesures de consommation

Vous avez chargé un modèle sur la carte stm testée. Vous souhaitez maintenant acquérir des données de consommation de courant au cours de l'inférence.

-   **brancher la carte testée à la plateforme de mesure** et **configurer le logiciel de mesure** pour aquérir des données (voir tutoriel d'utilisation de STM32CubePowerMonitor : https://docs.google.com/document/d/1QAFzkB69f2fFzQgcqn6YW7L7r4FzrJsOSsJqOU3MjXw/edit?usp=sharing)
-   **brancher la carte testée au PC** et s'assurer de la **position du jumper** de la carte pour que l'alimentation de celle-ci se fasse bien via la plateforme de mesure (pour stm32 : jumper en position E5V)
-   **Lancer l'acquisition** des données avec la plateforme, puis **exécuter le script** Communication.py.
-   **Attendre** que la carte testée réalise l'inférence avec le modèle uploadé avec les 50 images de test.
-   Arrêter l'acquisition des données, et sauvegarder les données acquises via STM32CubePowerMonitor dans le dossier 'AnalyseData'.

Vos mesures ont été sauvegardées. Vous pouvez maintenant passer à l'étape 'Pour visualiser les données'.

## Pour visualiser les données

Vous avez terminé l'acquisition des données, et vous avez un fichier au format .stpm contenant les mesures. Vous souhaitez maintenant visualiser les données.

-   **Ouvrir** le script HandleData.py. Entrer dans la variable 'modelName' le nom du modèle testé, puis **exécuter ce script**.
-   Après avoir créé les dossier qui vont contenir les fichiers de données, le script demande de déplacer le fichier \*.pckl qui doit etre dans 'Modules' vers le dossier 'AnalyseData/Data/" + modelName + "'", puis de mettre le fichier au format .stpm généré via stm32CubePowerMonitor dans 'AnalyseData', ce qui devrait être déjà fait.
-   **Appuyer sur Entree** après avoir fait ce que demande le script. Il reprend son exécution, et le dossier AnalyseData/Data/'modelName' devrait être rempli avec les données brutes dézippées (execution du script AnalyseData/UnzipStpm.py), et AnalyseData/Graphs/'modelName' devrait contenir des graphs permettant d'avoir un apercu des données mesurées, ainsi que des objets npy utilisés lors de la comparaison de modèles entre eux (execution du script Modules/Analyse.py).

Vous pouvez maintenant accéder au dossier AnalyseData/Graphs/'modelName', qui contient des images des graphes intéressants. Pour avoir les graphes directement, vous pouvez éditer la fonction analyse() dans Modules/Analyse.py. Cela ne nous intéressait pas car nous avons comparer les données de plusieurs modèles entre eux, ce qui est expliqué dans la partie suivante.

## Pour comparer les données de plusieurs modèles entre elles

Vous disposez des données traitées de plusieurs modèles, et vous souhaitez maintenant les comparer.

C'est le script VisualiseData.py qui vous permettra de comparer les données entre elles, mais il va falloir **l'éditer en fonction de ce que vous voulez visualiser**.

Visualiser des données est simplifié grâce à la fonction **createPlot()**, prenant en argument un titre de graph et la liste des modèles à plot dans le graph. Cette fonction se chargera aussi d'enregistrer le graph généré dans le dossier 'OutputData'. Il y a plusieurs utilisations de cette fonction dans le script VisualizeData.py.

# Nos Resultats

Dans cette partie, nous allons vous présenter les résultats de notre travail. Toutes les images présentées dans cette partie sont disponibles dans le dossier 'OutputData'.

Nous avons travaillé à partir d'un modèle de base ayant des performances correctes sur notre base de donnée : c6p22c16p22d120d84d43softmax. Dans la suite, ce modèle sera désigné comme "modèle de base", puisque nous avons testé plusieurs cas de figure en prenant celui-ci comme référence.

Dans un premier temps, afin de valider la pertinence de nos mesures, nous avons essayé de **mesurer 3 fois à des moments différents la consommation du modèle de base**.

![Screenshot](OutputData/Comparing3differentsmeasurementsofthesamemodel.png)

Ces mesures nous indiqué que :

-   Un léger offset en courant n'est pas significatif et est lié à la configuration de la plateforme.
-   Le pic de courant en fin d'inférence (ici à environ 105 ms) n'est pas significatif non plus, et doit plutôt être lié à la transmission du résultat de l'inférence au PC.

Nous avons ensuite voulu vérifier que nous obtenions bien des **mesures très distinctes** en simplifiant drastiquement le modèle. Le graphique suivant compare donc le modèle de base avec un modèle extrêmement simple : un GlobalMaxPooling, suivi d'une chouche dense de 43 neurones, et d'une couche d'activation softmax pour obtenir la sortie.

![Screenshot](OutputData/Basemodelvssmallestpossiblemodel.png)

Cet essai a fourni le résultat attendu : l'inférence est beaucoup plus rapide pour le modèle simple.

Puis nous avons essayé de connaitre l'influence des couches denses. Nous avons donc conduits 3 tests :

-   Ajouter des couches denses une par une à un modèle très simple.
    ![Screenshot](OutputData/Incrementaladditionoffullyconnectedlayersonsimplestmodel.png)

-   Modifier le nombre de neurones au sein d'une couche dense seule.
    ![Screenshot](OutputData/120vs1000neuronsinfullyconnectedlayer.png)

-   Reduire progressivement le nombre de couches denses du modèle de base.
    ![Screenshot](OutputData/Effectoftheremovaloffullyconnectedlayers.png)

Nous pouvons voir assez clairement les changements de consommation, et ceux-ci semblent assez cohérents : plus les couches denses comporte de paramètres, plus elles coûtent en énergie et en temps.

# Conclusion
