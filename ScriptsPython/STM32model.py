# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 10:34:17 2020

@author: Baptiste

Edited by Baptiste and Corentin to create custom models
"""


import numpy as np
from Modules.loadDataset import *
from tensorflow.keras.layers import *
from tensorflow.keras import datasets, layers, models
import tensorflow.keras as keras
import tensorflow as tf
import os
import Modules.easyTfRedactor as etfr
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "1"

# from nnom_utils import *


def Lenet5(classe=43, inputs_shape=[32, 32, 3]):

    inputs = tf.keras.Input(shape=inputs_shape)
    conv0 = tf.keras.layers.Conv2D(
        filters=1, kernel_size=5, activation='linear', use_bias=True)
    conv1 = tf.keras.layers.Conv2D(
        filters=6, kernel_size=5, activation='linear', use_bias=True)
    acti1 = tf.keras.layers.Activation('relu')
    pool1 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))
    conv2 = tf.keras.layers.Conv2D(
        filters=16, kernel_size=5, activation='linear', use_bias=True)
    acti2 = tf.keras.layers.Activation('relu')
    pool2 = tf.keras.layers.MaxPooling2D(
        pool_size=(2, 2))  # MaxPooling2D(pool_size=)
    flatten = tf.keras.layers.Flatten()
    dense1 = tf.keras.layers.Dense(
        units=120, activation='linear', use_bias=True)
    dense11 = tf.keras.layers.Dense(
        units=120, activation='linear', use_bias=True)
    dense111 = tf.keras.layers.Dense(
        units=120, activation='linear', use_bias=True)
    fat_dense1 = tf.keras.layers.Dense(
        units=1000, activation='linear', use_bias=True)
    fat_dense11 = tf.keras.layers.Dense(
        units=1000, activation='linear', use_bias=True)
    fat_dense111 = tf.keras.layers.Dense(
        units=1000, activation='linear', use_bias=True)
    acti3 = tf.keras.layers.Activation('relu')
    dense2 = tf.keras.layers.Dense(
        units=84, activation='linear', use_bias=True)
    acti4 = tf.keras.layers.Activation('relu')
    acti41 = tf.keras.layers.Activation('relu')
    dense3 = tf.keras.layers.Dense(
        units=classe, activation='linear', use_bias=True)
    acti5 = tf.keras.layers.Activation('softmax')
    acti6 = tf.keras.layers.Activation('softmax')

    actiNonSoftmax = tf.keras.layers.Activation('linear')
    gmp = tf.keras.layers.GlobalMaxPool2D()

    # x = conv1(inputs)
    # x = acti1(x)
    # x = pool1(x)
    # x = conv2(x)
    # x = acti2(x)
    # x = pool2(x)
    # x = flatten(x)
    # x = dense1(x)
    # x = acti3(x)
    # x = dense2(x)
    # x = acti4(x)
    # x = dense3(x)
    # x = acti5(x)
    # x = acti6(x)

    x = conv0(inputs)
    x = acti1(x)
    x = pool1(x)
    x = flatten(x)
    x = dense1(x)
    x = acti3(x)
    # x = dense11(x)
    # x = acti4(x)
    # x = dense111(x)
    # x = acti41(x)
    x = dense3(x)
    x = acti6(x)

    return tf.keras.Model(inputs=inputs, outputs=x)


def lr_schedule(epoch):
    lr = 1e-3
    if epoch >= 30:
        lr *= 1e-1
    elif epoch >= 10 and epoch < 20:
        lr *= 1e-1
    print('Learning rate: ', lr)
    return lr


BATCH_SIZE = 64
EPOCHS = 10


model = Lenet5(classe=43, inputs_shape=[32, 32, 3])

structure_model = "c1p22d120d43softmax"
# etfr_model = etfr.TfR_model(structure_model)
# model = etfr_model.compile()


train_images, train_labels, validation_images, validation_labels = load_gtsrb(
    path=os.getcwd()+os.sep)
np.save("GTSRB_xtest.npy", validation_images)
np.save("GTSRB_ytest.npy", validation_labels)
train_dataset = tf.data.Dataset.from_tensor_slices(
    (train_images, train_labels))
validation_dataset = tf.data.Dataset.from_tensor_slices(
    (validation_images, validation_labels))
model.compile(loss=tf.keras.losses.categorical_crossentropy, optimizer=tf.keras.optimizers.Adam(
    learning_rate=lr_schedule(0)), metrics=['accuracy'])

lr_scheduler = tf.keras.callbacks.LearningRateScheduler(lr_schedule)

lr_reducer = tf.keras.callbacks.ReduceLROnPlateau(factor=np.sqrt(0.1),
                                                  cooldown=0,
                                                  patience=5,
                                                  min_lr=0.5e-6)

callbacks = [lr_reducer, lr_scheduler]

# callbacks = [lr_reducer]


history = model.fit(train_dataset.batch(BATCH_SIZE), epochs=EPOCHS,
                    validation_data=validation_dataset.batch(BATCH_SIZE),
                    callbacks=callbacks, verbose=2)

model.evaluate(validation_images, validation_labels)
model.save('lenet5_'+structure_model+'.h5')
model.summary()
