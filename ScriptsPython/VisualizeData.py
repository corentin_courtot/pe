#!/usr/bin/env python

""" 
@author: Baptiste Lefevre and Corentin Courtot

Opens all graphs generated before in /AnalyseData/Graphs. 
Sorts the graphs in different plots in order to visualize data easily.
"""


import os
import numpy as np
import matplotlib.pylab as plt

# stores in dirs all directory all models with graph data in /AnalyseData/Graphs
dirs = []
for directory in os.listdir('./AnalyseData/Graphs'):
    if directory[0] != '_':
        dirs.append(directory)

# stores in Ts (Times) and Is (current Intensities) inference consumption data for every model
# writing "plt.plot(Ts[0], Is[0]) would plot the consumption data for the 1st model in the /AnalyseData/Graphs folder"
Ts = []
Is = []
for dir in dirs:
    t = np.load('./AnalyseData/Graphs/'+dir+'/T_typic.npy')
    i = np.load('./AnalyseData/Graphs/'+dir+'/I_typic.npy')
    Ts.append(t)
    Is.append(i)


def createPlot(title, models, nbPoints=5000):
    """
    Creates a figure and plots all consumption data of every model in models in one graph.
    Adds a legend, titles, and saves the figure in 'OutputData' folder.
    nbPoints is used in case the inference of the models is much shorter than 500 ms : setting it to 1200 will only display the first 120 ms of recording.
    """

    plt.figure(title, figsize=(12, 9))
    plt.title(title)
    for i in range(len(Ts)):
        if dirs[i] in models:
            plt.plot(Ts[i][:nbPoints], Is[i][:nbPoints], label=dirs[i])
    plt.legend()
    plt.xlabel('Time (ms)')
    plt.ylabel('Consumption (uA)')
    # removing spaces in title to not have spaces in saved file name
    plt.savefig('OutputData/' + title.replace(' ', '') + '.png')
    plt.show()

# Until the end of the script, select a few models with comparable data and plot them in the same graph


# 1st plot with all models measured
createPlot("All measured models", dirs)

# 2nd plot : base model measured 3 times to test consistency
createPlot("Comparing 3 differents measurements of the same model", [
           'c6p22c16p22d120d84d43softmax', 'c6p22c16p22d120d84d43softmax_v2', 'c6p22c16p22d120d84d43softmax_v3'],
           1300)

# 3rd plot : comparing base model with the smallest possible model with our data set
createPlot("Base model vs smallest possible model", [
           'c6p22c16p22d120d84d43softmax', 'GMPd43softmax'],
           1300)

# 4th plot : changing activation fonction
createPlot("Linear vs softmax", [
           'c6p22c16p22d120d84d43softmax', 'c6p22c16p22d120d84d43linear'],
           1300)

# 5th plot : adding one more softmax to base model
createPlot("Base model vs base model with softmax applied twice", [
           'c6p22c16p22d120d84d43softmax', 'c6p22c16p22d120d84d43softmax2'],
           1300)

# 6th plot : removing fully connected layers to base model
createPlot("Effect of the removal of fully connected layers", [
           'c6p22c16p22d120d84d43softmax', 'c6p22c16p22d120d43softmax', 'c6p22c16p22d43softmax'],
           1300)

# 7th plot : incremental addition of fully connected layers to smallest model
createPlot("Incremental addition of fully connected layers on simplest model", [
           'GMPd43softmax', 'GMPd120d43softmax', 'GMPd120d120d43softmax', 'GMPd120d120d120d43softmax'],
           300)

# 8th plot : 120 vs 1000 fully connected layers
createPlot("120 vs 1000 neurons in fully connected layer", [
           'GMPd1000d43softmax', 'GMPd120d43softmax'],
           300)

# 9th plot : Convolution(filter = 1, kernel_size = 5) vs GlobalMaxPooling
createPlot("Effects of GlobalMaxPooling vs Convolution(filter = 1, kernel_size = 5)", [
           'c1p22d120d43softmax', 'GMPd120d43softmax'],
           400)

# 10th plot : Modifying conv layers on base model
createPlot("Modifying conv layers on base model", [
           'c6p22c16p22d120d84d43softmax', 'c32p22c16p22d120d84d43softmax', 'c6p22c16p22c32d120d84d43softmax'])

# 11th plot : base model effect of 8bit quantization
createPlot("Effects of 8bit quantization on base model", [
           'quantized8_c6p22c16p22d120d84d43softmax', 'c6p22c16p22d120d84d43softmax'],
           1300)

# 12th plot : 2 other 8bit quantized models that could not fit in chip otherwise
createPlot("2 other quantized models", [
           'quantized8_c6p22d120d120d43softmax', 'quantized8_GMPd120d120d120d43softmax'],
           1300)
